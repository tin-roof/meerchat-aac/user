package user

import (
	"context"
	"errors"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

type Family struct {
	CreatedAt  time.Time    `json:"createdAt,omitempty"`
	ID         *uuid.UUID   `json:"id,omitempty"`
	UpdatedAt  time.Time    `json:"updatedAt,omitempty"`
	Membership Membership   `json:"membership,omitempty"`
	Members    []Membership `json:"members,omitempty"`
}

// @TODO: membership is being overloaded here. need to refactor how all the parts fit together here
type Membership struct {
	AcceptedInvite bool        `json:"acceptedInvite"`
	CreatedAt      *time.Time  `json:"createdAt,omitempty"`
	Email          *string     `json:"email,omitempty"`
	Family         *uuid.UUID  `json:"family,omitempty"`
	ID             *uuid.UUID  `json:"id,omitempty"`
	InviteCode     *uuid.UUID  `json:"inviteCode,omitempty"`
	IsAdmin        bool        `json:"isAdmin"`
	Name           string      `json:"name,omitempty"`
	Subscription   *uuid.UUID  `json:"subscription,omitempty"`
	Therapist      *Membership `json:"therapist,omitempty"`
	UpdatedAt      *time.Time  `json:"updatedAt,omitempty"`
	User           *uuid.UUID  `json:"user,omitempty"`
	HomeBoardID    *uuid.UUID  `json:"homeBoardID,omitempty"`
}

// DeleteFamilyMember removes a family member from the system
func (u *User) DeleteFamilyMember(ctx context.Context, db database.DB, m Membership) (*User, error) {
	ctx, log := logger.New(ctx, "user.User.DeleteFamilyMember", logger.WithField("userID", u.ID))

	log.WithField("member", m).Info(ctx, "removing family member")

	// look up the user if it hasn't been already
	if u.CreatedAt.IsZero() {
		u, err := u.lookup(ctx, db)
		if err != nil {
			return u, err
		}
	}

	// start a transaction
	if err := db.Transaction(ctx); err != nil {
		return u, err
	}
	// roll back the transaction if there is an error
	defer db.TX.Rollback(ctx)

	if m.Email != nil && *m.Email != "" {
		// profiles with emails can just be unlinked and a new family created for them
		log.WithField("member", m).Info(ctx, "moving member to their own family")

		// create the new family
		var familyID uuid.UUID
		if err := db.TX.Conn.QueryRow(ctx, familyCreateQuery).Scan(&familyID); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to create new family")
			return nil, err
		}

		// create the family member link
		if _, err := db.TX.Conn.Exec(ctx, familyMemberCreateQuery, familyID, m.User, true); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to create new family member")
			return nil, err
		}

		// delete old family link
		if _, err := db.TX.Conn.Exec(ctx, familyMemberDeleteQuery, u.Family.ID, m.User); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to delete old family listing")
			return nil, err
		}

		// remove the users subscription
		if _, err := db.TX.Conn.Exec(ctx, userRemoveSubscriptionQuery, m.User); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to remove the users subscription")
			return nil, err
		}
	} else {
		// profiles without emails should be removed completely
		log.WithField("member", m).Info(ctx, "fully removing profile")

		// delete family member record
		if _, err := db.TX.Conn.Exec(ctx, familyMemberDeleteQuery, u.Family.ID, m.User); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to remove family member")
			return nil, err
		}

		// delete settings
		if _, err := db.TX.Conn.Exec(ctx, settingsDeleteQuery, m.User); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to delete user settings")
			return nil, err
		}

		// delete pages
		if _, err := db.TX.Conn.Exec(ctx, pagesDeleteQuery, m.User); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to delete pages")
			return nil, err
		}

		// delete therapist links
		if _, err := db.TX.Conn.Exec(ctx, therapistLinkDeleteQuery, m.User); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to delete family link")
			return nil, err
		}

		// delete user
		if _, err := db.TX.Conn.Exec(ctx, userDeleteQuery, m.User); err != nil {
			log.WithError(err).WithField("member", m).Info(ctx, "failed to delete user")
			return nil, err
		}

		// @TODO: figure out the best way to handle file cleanup for images/etc
	}

	// save all the changes
	if err := db.TX.Commit(ctx); err != nil {
		return nil, err
	}

	log.WithField("member", m).Info(ctx, "successfully handled removed family member")
	return u, nil
}

// FamilyMembers finds all the family members in the same family as the user
func (u *User) FamilyMembers(ctx context.Context, db database.DB) (*User, error) {
	ctx, log := logger.New(ctx, "user.User.FamilyMembers", logger.WithField("userID", u.ID))

	// look up the user if it hasn't been already
	if u.CreatedAt.IsZero() {
		u, err := u.lookup(ctx, db)
		if err != nil {
			return u, err
		}
	}

	// lookup the family members
	rows, err := db.Conn.Query(ctx, familyMemberLookupQuery, u.Family.ID)
	if err != nil {
		log.WithError(err).Info(ctx, "error running family member lookup query")
		return u, err
	}

	// build the family member list
	members := []Membership{}
	for rows.Next() {
		var member Membership
		if err := rows.Scan(
			&member.User,
			&member.CreatedAt,
			&member.Subscription,
			&member.Email,
			&member.Name,
			&member.InviteCode,
			&member.AcceptedInvite,
			&member.IsAdmin,
			&member.HomeBoardID,
		); err != nil {
			log.WithError(err).Info(ctx, "error scanning family member lookup query row")
			continue
		}

		members = append(members, member)
	}

	// check to see if the user has a therapist
	for i, member := range members {
		var therapist Membership
		if err := db.Conn.QueryRow(ctx, userTherapistDetailsQuery, member.User).Scan(
			&therapist.ID,
			&therapist.Name); err != nil {
			log.WithError(err).WithField("member", member.User).Info(ctx, "error running therapist lookup query")
		} else {
			members[i].Therapist = &therapist
		}
	}

	u.Family.Members = members

	return u, nil
}

// NewFamilyMember creates a new family member and all the default parts
func (u *User) NewFamilyMember(ctx context.Context, db database.DB, m Membership) (*User, error) {
	ctx, log := logger.New(
		ctx,
		"user.User.NewFamilyMember",
		logger.WithField("userID", u.ID),
	)

	// look up the user if it hasn't been already
	if u.CreatedAt.IsZero() {
		u, err := u.lookup(ctx, db)
		if err != nil {
			return u, err
		}
	}

	// start a transaction
	if err := db.Transaction(ctx); err != nil {
		return u, err
	}
	// roll back the transaction if there is an error
	defer db.TX.Rollback(ctx)

	// create user
	var memberID uuid.UUID
	if err := db.TX.Conn.QueryRow(
		ctx,
		userCreateWithSubscriptionQuery,
		m.Email,
		m.Name,
		m.Subscription,
	).Scan(&memberID); err != nil {
		log.WithError(err).Info(ctx, "failed to create user")
		return nil, err
	}

	// create the family member link
	if _, err := db.TX.Conn.Exec(ctx, familyMemberCreateQuery, u.Family.ID, memberID, m.IsAdmin); err != nil {
		log.WithError(err).Info(ctx, "failed to create family member")
		return nil, err
	}

	// create the default app settings
	if _, err := db.TX.Conn.Exec(ctx, settingsDefaultCreateQuery, memberID); err != nil {
		log.WithError(err).Info(ctx, "failed to create user settings")
		return nil, err
	}

	// create the default page
	if _, err := db.TX.Conn.Exec(ctx, pageDefaultCreateQuery, memberID); err != nil {
		log.WithError(err).Info(ctx, "failed to create default home page")
		return nil, err
	}

	// link therapist if the id belongs to a therapist
	if m.Therapist != nil {
		if *m.Therapist.ID != uuid.Nil {
			var therapist User
			if err := db.TX.Conn.QueryRow(
				ctx,
				userLookupByIDQuery,
				m.Therapist.ID,
			).Scan(
				&therapist.ID,
				&therapist.CreatedAt,
				&therapist.UpdatedAt,
				&therapist.SubscriptionID,
				&therapist.Email,
				&therapist.Name,
				&therapist.IsTherapist,
			); err != nil {
				log.WithError(err).Info(ctx, "failed to lookup therapist")
				return nil, err
			}

			// link the user to the therapist account
			if therapist.IsTherapist {
				if _, err := db.TX.Conn.Exec(ctx, therapistLinkCreateQuery, therapist.ID, memberID); err != nil {
					log.WithError(err).Info(ctx, "failed to link therapist")
					return nil, err
				}
			} else {
				err := errors.New("therapist id is not a valid therapist")
				log.WithError(err).Info(ctx, "user is not a therapist")
				return nil, err
			}
		}
	}

	// save all the changes
	if err := db.TX.Commit(ctx); err != nil {
		return nil, err
	}

	return u, nil
}

// UpdateFamilyMember updates the details of a family member
func (u *User) UpdateFamilyMember(ctx context.Context, db database.DB, m Membership) (*User, error) {
	ctx, log := logger.New(
		ctx,
		"user.User.UpdateFamilyMember",
		logger.WithField("userID", u.ID),
	)

	log.WithField("member", m).Info(ctx, "updating family member")

	// look up the user if it hasn't been already
	if u.CreatedAt.IsZero() {
		u, err := u.lookup(ctx, db)
		if err != nil {
			return u, err
		}
	}

	// start a transaction
	if err := db.Transaction(ctx); err != nil {
		return u, err
	}
	// roll back the transaction if there is an error
	defer db.TX.Rollback(ctx)

	// update the user details
	if _, err := db.TX.Conn.Exec(ctx, userUpdateDetailsQuery, m.Name, m.Email, m.User); err != nil {
		log.WithError(err).Info(ctx, "failed to update user details")
		return nil, err
	}

	// update family member details
	if _, err := db.TX.Conn.Exec(ctx, familyMemberUpdateQuery, m.IsAdmin, u.Family.ID, m.User); err != nil {
		log.WithError(err).Info(ctx, "failed to update family member")
		return nil, err
	}

	// link therapist if the id belongs to a therapist
	if m.Therapist != nil {
		if *m.Therapist.ID != uuid.Nil {
			var therapist User
			if err := db.TX.Conn.QueryRow(
				ctx,
				userLookupByIDQuery,
				m.Therapist.ID,
			).Scan(
				&therapist.ID,
				&therapist.CreatedAt,
				&therapist.UpdatedAt,
				&therapist.SubscriptionID,
				&therapist.Email,
				&therapist.Name,
				&therapist.IsTherapist,
			); err != nil {
				log.WithError(err).Info(ctx, "failed to lookup therapist")
				return nil, err
			}

			// link the user to the therapist account
			if therapist.IsTherapist {
				// unlink any previous therapists for the member
				if _, err := db.TX.Conn.Exec(ctx, therapistLinksDeactivateQuery, m.User); err != nil {
					log.WithError(err).Info(ctx, "unlinking therapist from user err")
					return nil, err
				}

				if _, err := db.TX.Conn.Exec(ctx, therapistLinkCreateQuery, therapist.ID, m.User); err != nil {
					log.WithError(err).Info(ctx, "linking therapist to user err")
					return nil, err
				}
			} else {
				err := errors.New("therapist id is not a valid therapist")
				log.WithError(err).Info(ctx, "linking therapist to user err")
				return nil, err
			}
		} else {
			// unlink any previous therapists for the member
			if _, err := db.TX.Conn.Exec(ctx, therapistLinksDeactivateQuery, m.User); err != nil {
				log.WithError(err).Info(ctx, "unlinking therapist from user err")
				return nil, err
			}
		}
	} else {
		// unlink any previous therapists for the member
		if _, err := db.TX.Conn.Exec(ctx, therapistLinksDeactivateQuery, m.User); err != nil {
			log.WithError(err).Info(ctx, "unlinking therapist from user err")
			return nil, err
		}
	}

	// save all the changes
	if err := db.TX.Commit(ctx); err != nil {
		return nil, err
	}

	log.WithField("member", m).Info(ctx, "successfully updated family member")

	return u, nil
}
