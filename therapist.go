package user

import (
	"context"
	"errors"

	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// TherapistLinkList finds all the patients linked to a therapist
func (u *User) TherapistLinkList(ctx context.Context, db database.DB) (*User, error) {
	ctx, log := logger.New(ctx, "user.User.TherapistPatients", logger.WithField("userID", u.ID))

	// look up the user if it hasn't been already
	if u.CreatedAt.IsZero() {
		u, err := u.lookup(ctx, db)
		if err != nil {
			return u, err
		}
	}

	// skip function if the user is not a therapist
	if !u.IsTherapist {
		return u, errors.New("user is not a therapist")
	}

	// lookup the family members
	rows, err := db.Conn.Query(ctx, therapistLinkListQuery, u.ID)
	if err != nil {
		log.WithError(err).Info(ctx, "error running therapist patient lookup query")
		return u, err
	}

	// build the page list
	clients := []Membership{}
	for rows.Next() {
		var member Membership
		if err := rows.Scan(
			&member.User,
			&member.CreatedAt,
			&member.Subscription,
			&member.Email,
			&member.Name,
			&member.HomeBoardID,
		); err != nil {
			log.WithError(err).Info(ctx, "error scanning therapist link lookup query row")
			continue
		}

		clients = append(clients, member)
	}

	u.Clients = clients

	return u, nil
}
