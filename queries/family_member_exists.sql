-- lookup a family member based on profile ID
SELECT 
	fm."id" 
FROM 
	"family_members" fm 
WHERE 
	fm."family" = $1
	AND fm."user" = $2;