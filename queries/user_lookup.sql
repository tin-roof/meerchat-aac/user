SELECT 
	u."id" as user_id, 
	u."created_at" as user_created_at, 
	u."updated_at" as user_updated_at,
	u."subscription" as user_subscription_id,
    u."therapist" as is_therapist,
	fm."id" as family_member_id, 
	fm."created_at" as family_member_created_at, 
	fm."updated_at" as family_member_updated_at, 
	fm."invite_code" as family_member_invite_code, 
	fm."accepted_invite" as family_member_accepted_invite, 
	fm."is_admin" as family_member_is_admin,
	f."id" as family_id, 
	f."created_at" as family_created_at, 
	f."updated_at" as family_updated_at,
    p."id" as home_board_id
FROM 
	"user" u, 
	"family_members" fm, 
	"family" f,
    "page" p
WHERE 
	fm."user" = u."id"
    AND f."id" = fm."family"
    AND p."user" = u."id"
    AND p."title" = 'home'
    AND u."email" = $1;