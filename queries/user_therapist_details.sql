-- lookup therapist information for a user
SELECT 
    u."id",
    u."name"
FROM 
    "user" u, 
    "user_therapist" ut 
WHERE 
    u."id" = ut."therapist_id"
    AND ut."active" = true
    AND ut."user_id" = $1;