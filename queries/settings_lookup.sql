-- lookup settings for the user
SELECT 
    s.settings
FROM 
    "settings" s,
    "user" u
WHERE
    s.user = u.id
    AND u.email = $1