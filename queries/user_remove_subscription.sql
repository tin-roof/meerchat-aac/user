-- remove the subscription for a user
UPDATE "user" SET "subscription" = DEFAULT WHERE "id" = $1;