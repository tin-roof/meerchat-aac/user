-- delete a family member record
DELETE FROM "family_members" WHERE "family" = $1 AND "user" = $2;