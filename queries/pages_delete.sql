-- delete pages records for a user
DELETE FROM "page" WHERE "user" = $1 AND "published" = false;