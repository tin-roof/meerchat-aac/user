-- lookup pages for the user
SELECT 
    p.title,
    p.buttons,
    p.grid
FROM 
    "page" p
WHERE
    p."user" = $1