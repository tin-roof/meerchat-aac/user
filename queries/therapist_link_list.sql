-- lookup all the users that a therapist is linked to
SELECT 
    u."id" as user_id, 
    u."created_at" as user_created_at, 
    u."subscription" as user_subscription_id,
    u."email" as user_email,
    u."name" as user_name,
    p."id" as home_board_id
FROM 
    "user" u, 
    "user_therapist" ut,
    "page" p
WHERE 
    u."id" = ut."user_id"
    AND p."user" = ut."user_id"
    AND p."title" = 'home'
    AND ut."active" = true
    AND ut."therapist_id" = $1;