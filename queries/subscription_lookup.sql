-- lookup the subscription for a user
SELECT 
    s."id",
    s."created_at",
    s."updated_at",
    s."expires_on",
    s."family",
    s."seats" as "total_seats",
    COUNT(DISTINCT u."id") as "used_seats"
FROM 
    "subscription" s,
    "user" u
WHERE 
    u."subscription" = s."id" AND
    s."id" = $1
GROUP BY 1
LIMIT 1;