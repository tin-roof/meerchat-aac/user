-- lookup all the members of a family
SELECT 
    u."id" as user_id, 
    u."created_at" as user_created_at, 
    u."subscription" as user_subscription_id,
    u."email" as user_email,
    u."name" as user_name, 
    fm."invite_code" as family_member_invite_code, 
    fm."accepted_invite" as family_member_accepted_invite, 
    fm."is_admin" as family_member_is_admin,
    p."id" as home_board_id
FROM 
    "user" u, 
    "family_members" fm,
    "page" p
WHERE 
    fm."user" = u."id"
    AND p."user" = u."id"
    AND p."title" = 'home'
    AND fm."family" = $1;