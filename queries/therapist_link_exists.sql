-- lookup to see if a link between a therapist and profile id exists
SELECT 
	ut."id" 
FROM 
	"user_therapist" ut 
WHERE 
	ut."active" = true
	AND ut."therapist_id" = $1
	AND ut."user_id" = $2;