-- deactivate all therapist links for a user
UPDATE "user_therapist" SET "active" = false WHERE "active" = true AND "user_id" = $1;