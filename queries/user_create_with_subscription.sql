-- create a new user record
INSERT INTO "user" ("email", "name", "subscription") VALUES ($1, $2, $3) RETURNING id;