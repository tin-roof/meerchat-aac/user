INSERT INTO "page" ("user", "title", "buttons") VALUES($1, $2, $3) 
ON CONFLICT ("user", "title") 
DO 
   UPDATE SET "buttons" = $3, "updated_at" = CURRENT_TIMESTAMP;