-- lookup the base user info by id
SELECT 
    u."id",
    u."created_at", 
    u."updated_at",
    u."subscription",
    u."email",
    u."name",
    u."therapist"
FROM 
    "user" u
WHERE 
    u."id" = $1;