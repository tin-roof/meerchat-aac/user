-- update details in a family member record
UPDATE "family_members" SET "is_admin" = $1 WHERE "family" = $2 AND "user" = $3;