package user

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

// IsLinked looks to see if the user is linked in some way to the profile ID provided
// - to be linked the user needs to be an admin family member OR a linked therapist
func (u *User) IsLinked(ctx context.Context, db database.DB, profileID string) bool {
	ctx, log := logger.New(ctx, "user.User.IsLinked", logger.WithField("userID", u.ID))

	// look up the user if it hasn't been already
	if u.CreatedAt.IsZero() {
		_, err := u.lookup(ctx, db)
		if err != nil {
			return false
		}
	}

	// return early if the ids are the same
	if u.ID.String() == profileID {
		return true
	}

	var linkedID uuid.UUID

	// check family members for the given profile id
	err := db.Conn.QueryRow(ctx, familyMemberExistsQuery, u.Family.ID, profileID).
		Scan(&linkedID)
	if err != nil && !errors.Is(err, pgx.ErrNoRows) {
		log.WithError(err).Info(ctx, "error looking for family member")
		return false
	}

	if linkedID != uuid.Nil {
		return true
	}

	// check therapist link for the given profile id
	if u.IsTherapist {
		err := db.Conn.QueryRow(ctx, therapistLinkExistsQuery, u.ID, profileID).
			Scan(&linkedID)
		if err != nil && !errors.Is(err, pgx.ErrNoRows) {
			log.WithError(err).Info(ctx, "error looking for therapist link")
			return false
		}

		if linkedID != uuid.Nil {
			return true
		}
	}

	return false
}
