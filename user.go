package user

import (
	"context"
	_ "embed"
	"encoding/base64"
	"encoding/json"
	"errors"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"gitlab.com/tin-roof/meerchat-aac/database"
)

type token struct {
	ISS           string `json:"iss"`
	NBF           int    `json:"nbf"`
	AUD           string `json:"aud"`
	SUB           string `json:"sub"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	AZP           string `json:"azp"`
	Name          string `json:"name"`
	Picture       string `json:"picture"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	IAT           int    `json:"iat"`
	EXP           int    `json:"exp"`
	JTI           string `json:"jti"`
}

type User struct {
	Clients        []Membership  `json:"clients,omitempty"`
	CreatedAt      time.Time     `json:"createdAt,omitempty"`
	Email          string        `json:"email,omitempty"`
	Family         Family        `json:"family,omitempty"`
	ID             uuid.UUID     `json:"id,omitempty"`
	IsTherapist    bool          `json:"isTherapist"`
	Name           string        `json:"name,omitempty"`
	Picture        string        `json:"picture,omitempty"`
	Subscription   *Subscription `json:"subscription,omitempty"`
	SubscriptionID uuid.UUID     `json:"subscriptionID,omitempty"`
	UpdatedAt      time.Time     `json:"updatedAt,omitempty"`
	Verified       bool          `json:"verified,omitempty"`
	HomeBoardID    *uuid.UUID    `json:"homeBoardID,omitempty"`
}

// load the queries needed for user setup
var (
	//go:embed queries/family_create.sql
	familyCreateQuery string

	//go:embed queries/family_member_create.sql
	familyMemberCreateQuery string

	//go:embed queries/family_member_delete.sql
	familyMemberDeleteQuery string

	//go:embed queries/family_member_exists.sql
	familyMemberExistsQuery string

	//go:embed queries/family_member_update.sql
	familyMemberUpdateQuery string

	//go:embed queries/family_members_list.sql
	familyMemberLookupQuery string

	//go:embed queries/page_default_create.sql
	pageDefaultCreateQuery string

	//go:embed queries/page_save.sql
	pageSaveQuery string

	//go:embed queries/page_lookup.sql
	pagesLookupQuery string

	//go:embed queries/pages_delete.sql
	pagesDeleteQuery string

	//go:embed queries/settings_default_create.sql
	settingsDefaultCreateQuery string

	//go:embed queries/settings_delete.sql
	settingsDeleteQuery string

	//go:embed queries/settings_lookup.sql
	settingsLookupQuery string

	//go:embed queries/subscription_lookup.sql
	subscriptionLookupQuery string

	//go:embed queries/therapist_link_create.sql
	therapistLinkCreateQuery string

	//go:embed queries/therapist_link_delete.sql
	therapistLinkDeleteQuery string

	//go:embed queries/therapist_link_exists.sql
	therapistLinkExistsQuery string

	//go:embed queries/therapist_link_list.sql
	therapistLinkListQuery string

	//go:embed queries/therapist_links_deactivate.sql
	therapistLinksDeactivateQuery string

	//go:embed queries/user_create.sql
	userCreateQuery string

	//go:embed queries/user_create_with_subscription.sql
	userCreateWithSubscriptionQuery string

	//go:embed queries/user_delete.sql
	userDeleteQuery string

	//go:embed queries/user_lookup.sql
	userLookupQuery string

	//go:embed queries/user_lookup_by_id.sql
	userLookupByIDQuery string

	//go:embed queries/user_remove_subscription.sql
	userRemoveSubscriptionQuery string

	//go:embed queries/user_therapist_details.sql
	userTherapistDetailsQuery string

	//go:embed queries/user_update_details.sql
	userUpdateDetailsQuery string
)

// Verify reads a json web token and makes sure its valid/verified/not expired
func Verify(ctx context.Context, t string) (*User, error) {

	// make sure to add the padding to the token so it will validate
	if i := len(t) % 4; i != 0 {
		t += strings.Repeat("=", 4-i)
	}

	// parse the token and make sure everything is good
	data, dErr := base64.StdEncoding.DecodeString(t)
	if dErr != nil {
		return nil, dErr
	}

	var details token
	jErr := json.Unmarshal(data, &details)
	if jErr != nil {
		return nil, jErr
	}

	// if the email is unverified error out
	if !details.EmailVerified {
		return nil, errors.New("email is unverified")
	}

	// if the token is valid set some users details and return
	return &User{
		Email:    details.Email,
		Name:     details.Name,
		Picture:  details.Picture,
		Verified: details.EmailVerified,
	}, nil
}

// FindOrCreate finds a user or creates a new one if one doesn't exist
func (u *User) FindOrCreate(ctx context.Context, db database.DB) (*User, error) {
	// lookup the user to see if one exists
	var lErr error
	u, lErr = u.lookup(ctx, db)
	if lErr != nil && !errors.Is(lErr, pgx.ErrNoRows) {
		return u, lErr
	}

	// if the user doesn't have an id create a new user
	if u.ID == uuid.Nil {
		return u.create(ctx, db)
	}

	return u, nil
}

// GetID finds the users ID by email
func (u *User) GetID(ctx context.Context, db database.DB) (*User, error) {
	err := db.Conn.QueryRow(
		ctx,
		`SELECT "id" FROM "user" WHERE "email" = $1`,
		u.Email,
	).Scan(&u.ID)
	if err != nil {
		return u, err
	}

	return u, nil
}

// lookup finds the user given its email address
func (u *User) lookup(ctx context.Context, db database.DB) (*User, error) {
	err := db.Conn.QueryRow(ctx, userLookupQuery, u.Email).
		Scan(
			&u.ID,
			&u.CreatedAt,
			&u.UpdatedAt,
			&u.SubscriptionID,
			&u.IsTherapist,
			&u.Family.Membership.ID,
			&u.Family.Membership.CreatedAt,
			&u.Family.Membership.UpdatedAt,
			&u.Family.Membership.InviteCode,
			&u.Family.Membership.AcceptedInvite,
			&u.Family.Membership.IsAdmin,
			&u.Family.ID,
			&u.Family.CreatedAt,
			&u.Family.UpdatedAt,
			&u.HomeBoardID,
		)
	if err != nil {
		return u, err
	}

	return u, nil
}

// create creates a new user in the system
func (u *User) create(ctx context.Context, db database.DB) (*User, error) {

	// start a transaction
	if err := db.Transaction(ctx); err != nil {
		return u, err
	}
	// roll back the transaction if there is an error
	defer db.TX.Rollback(ctx)

	// create the new user
	var userID uuid.UUID
	if err := db.TX.Conn.QueryRow(ctx, userCreateQuery, u.Email, u.Name).Scan(&userID); err != nil {
		return nil, err
	}

	// create the new family
	var familyID uuid.UUID
	if err := db.TX.Conn.QueryRow(ctx, familyCreateQuery).Scan(&familyID); err != nil {
		return nil, err
	}

	// create the family member link
	if _, err := db.TX.Conn.Exec(ctx, familyMemberCreateQuery, familyID, userID, true); err != nil {
		return nil, err
	}

	// create the default app settings
	if _, err := db.TX.Conn.Exec(ctx, settingsDefaultCreateQuery, userID); err != nil {
		return nil, err
	}

	// create the default page
	if _, err := db.TX.Conn.Exec(ctx, pageDefaultCreateQuery, userID); err != nil {
		return nil, err
	}

	// save all the changes
	if err := db.TX.Commit(ctx); err != nil {
		return nil, err
	}

	// after creation, look up the user so we pull all the details we need
	return u.lookup(ctx, db)
}
