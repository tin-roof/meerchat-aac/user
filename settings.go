package user

import (
	"context"
	_ "embed"

	"gitlab.com/tin-roof/meerchat-aac/database"
)

type Settings struct {
	BackgroundColor string `db:"backgroundColor" json:"backgroundColor"`
	BackgroundImage string `db:"backgroundImage" json:"backgroundImage"`
	CanEdit         bool   `db:"canEdit" json:"canEdit"`
	Grid            int    `db:"grid" json:"grid"`
}

// LookupSettings finds the users application settings
func (u *User) LookupSettings(ctx context.Context, db database.DB) (Settings, error) {
	// look up the settings
	var settings Settings
	err := db.Conn.QueryRow(ctx, settingsLookupQuery, u.Email).Scan(&settings)
	if err != nil {
		return settings, err
	}

	return settings, nil
}
