package user

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

type Subscription struct {
	CreatedAt  time.Time  `json:"createdAt,omitempty"`
	ExpiresOn  time.Time  `json:"expiresOn,omitempty"`
	FamilyID   *uuid.UUID `json:"familyID,omitempty"`
	ID         *uuid.UUID `json:"id,omitempty"`
	TotalSeats int        `json:"totalSeats"`
	UpdatedAt  time.Time  `json:"updated_at,omitempty"`
	UsedSeats  int        `json:"usedSeats"`
}

// LookupSubscription finds the subscription for the family
func (u *User) GetSubscription(ctx context.Context, db database.DB) (*User, error) {
	ctx, _ = logger.New(ctx, "user.User.Subscription", logger.WithField("userID", u.ID))

	// look up the user if it hasn't been already
	if u.CreatedAt.IsZero() {
		u, err := u.lookup(ctx, db)
		if err != nil {
			return u, err
		}
	}

	// if the user doesn't have a subscription, no point in looking it up
	if u.SubscriptionID == uuid.Nil {
		return u, nil
	}

	// lookup the subscription details
	var s Subscription
	err := db.Conn.QueryRow(ctx, subscriptionLookupQuery, u.SubscriptionID).
		Scan(
			&s.ID,
			&s.CreatedAt,
			&s.UpdatedAt,
			&s.ExpiresOn,
			&s.FamilyID,
			&s.TotalSeats,
			&s.UsedSeats,
		)
	if err != nil {
		return u, err
	}

	u.Subscription = &s

	return u, nil
}
