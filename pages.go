package user

import (
	"context"
	_ "embed"
	"encoding/json"

	"gitlab.com/tin-roof/meerchat-aac/database"
	"gitlab.com/tin-roof/meerchat-aac/logger"
)

type Pages map[string][]Button

type Button struct {
	AddToPhrase     bool   `db:"addToPhrase" json:"addToPhrase"`
	BackgroundColor string `db:"backgroundColor" json:"backgroundColor"`
	BorderColor     string `db:"borderColor" json:"borderColor"`
	ButtonID        string `db:"buttonID" json:"buttonID"`
	Hidden          bool   `db:"hidden" json:"hidden"`
	Image           string `db:"image" json:"image"`
	Label           string `db:"label" json:"label"`
	Link            string `db:"link" json:"link"`
	OpenLink        bool   `db:"openLink" json:"openLink"`
	SayNow          bool   `db:"sayNow" json:"sayNow"`
	Speech          string `db:"speech" json:"speech"`
	TextColor       string `db:"textColor" json:"textColor"`
}

// LookupPages finds the users pages
func (u *User) LookupPages(ctx context.Context, db database.DB) (Pages, error) {
	ctx, log := logger.New(ctx, "user.User.LookupPages", logger.WithField("userID", u.ID))

	// look up the pages
	rows, err := db.Conn.Query(ctx, pagesLookupQuery, u.ID)
	if err != nil {
		log.WithError(err).Info(ctx, "error running pages lookup query")
		return nil, err
	}

	// build the page list
	pages := make(Pages)
	for rows.Next() {
		var title string
		var buttons []Button
		var grid int // @TODO: not used yet - this will be part of the refactor to pull page settings back with pages not just the buttons - but it needs FE changes that I haven't done yet
		if err := rows.Scan(&title, &buttons, &grid); err != nil {
			log.WithError(err).Info(ctx, "error scanning page lookup query row")
			continue
		}
		pages[title] = buttons
	}

	return pages, nil
}

// SavePages saves the users pages back to the DB
func (u *User) SavePages(ctx context.Context, db database.DB, pages Pages) []error {
	ctx, logger := logger.New(ctx, "user.User.SavePages", logger.WithField("userID", u.ID))
	var errs []error

	// upsert the pages
	for title, buttons := range pages {
		logger.WithField("page", title).Info(ctx, "saving page")
		btns, _ := json.Marshal(buttons)

		results, err := db.Conn.Query(ctx, pageSaveQuery, u.ID, title, string(btns))
		if err != nil {
			logger.WithError(err).WithField("buttons", btns).Info(ctx, "failed to upsert page")
			errs = append(errs, err)
			continue
		}
		results.Close()
	}

	return errs
}
